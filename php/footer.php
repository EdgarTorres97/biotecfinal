<footer class="page-footer font-small teal pt-4">
    <div class="logo2">
    <a class="nav-link" href="playas"><img src="imagenes/logoBiotec2.png" title="logo" alt="logoBiotec"/> <span class="sr-only">(current)</span></a>
    </div>
    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">
      <!-- Grid row -->
      <div class="row">
        <!-- Grid column -->
        <div id="iz" class="col-md-4 mt-md-0 mt-6">
          <!-- Content -->
          <h5 class="text-uppercase font-weight-bold">Contactanos</h5>
          <ul>
              <li><i class="" style="font-size:24px"></i> Cancún Quintana Roo</li>
              <li><i class="" style="font-size:18px"></i>+52 (044) 9984075027</li>
              <li><i class="" style="font-size:18px"></i><a href=""> eath2497@gmail.com</a></li>
               <li><i class="" style="font-size:18px"></i><a href=""> lcehbalam@gmail.com</a></li>
            </ul>
        </div>
        <!-- Grid column -->
        <div class="col-md-6 mb-md-0 mb-3">
          <!-- Content -->
          <div class="creadores">
              <p>Universidad Tecnológica de Cancún<br>
                  Organismo Público Descentralizado del Gobierno del Estado de Quintana Roo<br>
                  Carretera Cancún-Aeropuerto, Km. 11.5, S.M. 299, Mz. 5, Lt 1<br>
                  Cancún, Quintana Roo, C.P. 77565<br>
                  Tel. 01 (998) 881 19 00 
                </p>
      </div>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
    <!-- Footer Text -->
    <div class="redes">
        <div class="sociales">
            <a href="https://www.facebook.com/UTdeCancun"><img src="imagenes/facebook.png" title="face" height="50" alt="Logo facebook"/></a>
                <a href="https://www.instagram.com/utcancun/"><img src="imagenes/insta.png" title="face" height="50" alt="Logo intagram"/></a>
                <a href="https://twitter.com/UTCancun"><img src="imagenes/twitter.png" title="face" height="45" alt="Logo twitter"/></a>
                <p class="copy">Biotec © 2019</p>
        </div>
        </div>
  </footer>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>