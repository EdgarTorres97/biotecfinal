<?php
    $nombre=$_POST["nombre"];
    $apeP=$_POST["apeP"];
    $apeM=$_POST["apeM"];
    $correo=$_POST["correo"];
    $telefono=$_POST["telefono"];
     $telefono;
    $usuario=$_POST["usuario"];
    $contra = $_POST["contra"];
    $vercontra = $_POST["verif"];
    $permiso = 3;
    if($contra == $vercontra){
    $clave = password_hash($contra, PASSWORD_DEFAULT, ['cost'=> 5]);
    try{
         require('conexion.php');
        $sql1="SELECT * FROM usuariosPrivilegiado WHERE usuario=:usu";
        $resultado1=$base->prepare($sql1);
        $resultado1->execute(array(":usu"=>$usuario));
        $result=$resultado1->fetchAll();
        if($result){
            echo '<script>
            alert("El usuario ya está registrado"); 
            window.history.go(-1);
            </script>';
        }else{
        
        $sql="INSERT INTO usuariosPrivilegiado(nombre,apellidoPaterno,apellidoMaterno,usuario,clave,permiso) VALUES (:nombre,:apeP,:apeM,:usuario,:clave,:per)";
        $resultado=$base->prepare($sql);
        $resultado->execute(array(":nombre"=>$nombre,":apeP"=>$apeP,":apeM"=>$apeM,":usuario"=>$usuario,":clave"=>$clave,":per"=>$permiso));
        
        $contusuario = $base->query("SELECT max(idusuario) from usuariosPrivilegiado");
        $cont = $contusuario->fetch();
        $con = $cont[0];
        $idusuario = (int)$con;
        if($idusuario<1){
            $idusuario = 1;
        }

        $sqlcorreo="INSERT INTO correos(idusuario,correo) VALUES(:idusuario,:correo)";
        $resultado2=$base->prepare($sqlcorreo);
        $resultado2->execute(array(":idusuario"=>$idusuario,":correo"=>$correo));

        $sqltelefono="INSERT INTO telefonos(idusuario,telefono) VALUES(:idusuario,:telefono)";
        $resultado3=$base->prepare($sqltelefono);
        $resultado3->execute(array(":idusuario"=>$idusuario,":telefono"=>$telefono));
        

        echo '<script>
            alert("Usuario registrado"); 
            window.history.go(-1);
            </script>';

        }
    }
    catch(exception $e){
        echo "Linea del error: " . $e->getmessage();
    }finally{
    $base=null;
    }
    }else{
    echo '<script>
		            alert("Las contraseñas no coinciden"); 
		            window.history.go(-1);
		            </script>';
    }
/*
    */
?>