
    <header>
    <nav class="navbar navbar-expand-lg navbar-light ">
              <div class="logo">
              <a class="nav-link" href="playas"><img src="imagenes/logoBiotec2.png" title="logo" alt="logoBiotec"/> <span class="sr-only">(current)</span></a>
              </div>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                  <a class="nav-link hov" href="nosotros">Nosotros <span class="sr-only">(current)</span></a>
                  </li>
                    <li class="nav-item active">
                          <a class="nav-link hov" href="playas">Playas <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                          <a class="nav-link hov" href="limpias">Mejor calificadas <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                          <a class="nav-link hov" href="convocatorias">Convocatorias <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link hov" href="contactanos">Contáctanos <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link hov dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php 
if(isset($_SESSION['usuario'])){
     echo $_SESSION['usuario']; 
}else{
echo "login"; 
}?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			<a class="dropdown-item" href="registro">Regístrate</a>
                      <a class="dropdown-item" href="ingresar">Ingresa</a>
                      <?php 
if(isset($_SESSION['usuario'])){
  echo '<a class="dropdown-item" href="perfil">Perfil</a>';
}?>
                      <a class="dropdown-item" href="cerrar">Cerrar Sesión</a>
                    </div>
                  </li>
                  <?php
                  if (($permiso == 1)or($permiso == 2)){
                    echo '
                    <li class="nav-item dropdown">
                      <a class="nav-link hov dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin Panel
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="insertaplaya">Nueva playa</a>
                        <a class="dropdown-item" href="comentarios">Comentarios recibidos</a>
                        <a class="dropdown-item" href="usuarios">Modificar usuarios</a>
                        <a class="dropdown-item" href="registroAdmin">Registro Admin</a>
                        <a class="dropdown-item" href="playaRegistro">Playas Registradas Admin</a>
                      </div>
                    </li>
                    
                    ';
                  }
                  ?>
                </ul>
              </div>
            </nav>            
    </header>
    <div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="imagenes/fondo.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <a class="btn btn-primary btn-lg active" href="vota" >Califica las playas <span class="sr-only">(current)</span></a>
        </div>
      </div>
      <div class="carousel-item">
        <img src="imagenes/img5.gif" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <a class="btn btn-primary btn-lg active" href="vota" >Califica las playas <span class="sr-only">(current)</span></a>
        </div>
      </div>
      <div class="carousel-item">
        <img src="imagenes/img6.gif" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <a class="btn btn-primary btn-lg active" href="vota" >Califica las playas <span class="sr-only">(current)</span></a>
        </div>
      </div>
      <div class="carousel-item">
        <img src="imagenes/img7.gif" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <a class="btn btn-primary btn-lg active" href="vota" >Califica las playas <span class="sr-only">(current)</span></a>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<div class="esc">
  <a class="btn btn-primary btn-lg active" href="vota" >Califica las playas <span class="sr-only">(current)</span></a>
</div>
    