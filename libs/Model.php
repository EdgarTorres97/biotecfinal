<?php
//include trae todo
//unicamente la parte que necesito lo trae
require_once ROOT_PATH.'/libs/DataBase.php';//importamos la clase database

//creando clase abstracta por que se va a utilzar para hacer herencia y no se puede instanciar.
//no se puede crear un objeto de ella
abstract class Model{
    //creando variable estatica protegida table para decir con que tabla estoy trabajando (generica)
    protected static $table=null;
    //creando funcion para traer todo los datos de la tabla
    public static function all(){
        //creando el metodo get instance
        $pdo= DataBase::getInstance();
        //accediendo a la tabla estatica table
        $sql='SELECT * FROM '.static::$table;
        $resultQry=$pdo->query($sql);
        //regresando el resultado de la consulta como arreglo
        return $resultQry->fetchAll(PDO::FETCH_OBJ);
    }
}