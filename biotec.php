<?php require('php/permiso.php');

if(isset($_SESSION['usuario'])){
    header('Location: cerrar');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/estilo6.css">
  <link rel="stylesheet" href="css/footer.css">

  <link rel="icon" href="imagenes/logoBiotec.ico">
  <title>Ingresa - Biotec</title>
</head>
  <body>
  <?php include("php/header.php"); ?>

    
      <div class="container">
      <div class="form__top">
            <h2>Ingresa a<span> Biotec</span></h2>
        </div>
      <form class="form__reg" action="php/validabiotec.php" method="POST">

            <input class="input" id="usuario" name="usuario" type="text" placeholder="Usuario " required autofocus>
            <input class="input" id="contra" name="clave" type="password" placeholder="Contraseña" required>
            <div class="container">
                <div class="btn__form">
                    <input class="btn__submit" type="submit" value="INGRESAR">
                    <a class="btn__submit" href="registro" role="button">Registrate</a>	
                </div>
            </div>
        </form>
      </div>
      <?php include("php/footer.php"); ?>

</body>
</html>