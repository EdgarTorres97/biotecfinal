<?php
//creando clase que hereda de Controller
require_once ROOT_PATH . '/libs/Controller.php' ;
require_once ROOT_PATH . '/libs/View.php' ;
require_once ROOT_PATH . '/model/topPlayas.php' ;


class topPlayasController extends Controller{
    //creando funcion getIndex para invocar al metodo all
    public function getTopPlayas(){
        $topPlayas=topPlayas::all();
        //regresando la vista regresando como parametro la lsita de los reistros de la consulta
        //a la vista le indicamos el html y los registros de la consulta
        return new View('playa/topPlayas',['topPlayas'=>$topPlayas]);
    }
}
