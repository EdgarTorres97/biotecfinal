<?php
//creando clase que hereda de Controller
require_once ROOT_PATH . '/libs/Controller.php' ;
require_once ROOT_PATH . '/libs/View.php' ;

class registroAdminController extends Controller{
    //creando funcion getIndex para invocar al metodo all
    public function getRegistroAdmin(){
        return new View('playa/registroAdmin');
    }
}
