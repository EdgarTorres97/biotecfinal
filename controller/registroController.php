<?php
//creando clase que hereda de Controller
require_once ROOT_PATH . '/libs/Controller.php' ;
require_once ROOT_PATH . '/libs/View.php' ;

class registroController extends Controller{
    //creando funcion getIndex para invocar al metodo all
    public function getRegistro(){
        return new View('playa/registro');
    }
}
