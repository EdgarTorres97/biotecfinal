<?php
define("ROOT_PATH",__DIR__);

require_once ROOT_PATH . '/controller/votaplayaController.php' ;
require_once ROOT_PATH . '/controller/cerrarController.php' ;
require_once ROOT_PATH . '/controller/comenController.php' ;
require_once ROOT_PATH . '/controller/contactanosController.php' ;
require_once ROOT_PATH . '/controller/convoController.php' ;
require_once ROOT_PATH . '/controller/ingresarController.php' ;
require_once ROOT_PATH . '/controller/insertaplayaController.php' ;
require_once ROOT_PATH . '/controller/insertaVotController.php' ;
require_once ROOT_PATH . '/controller/nosotrosController.php' ;
require_once ROOT_PATH . '/controller/perfilController.php' ;
require_once ROOT_PATH . '/controller/registroController.php' ;
require_once ROOT_PATH . '/controller/usuariomodController.php' ;
require_once ROOT_PATH . '/controller/usuariosController.php' ;
require_once ROOT_PATH . '/controller/registroAdminController.php' ;
require_once ROOT_PATH . '/controller/pRegistroController.php' ;
require_once ROOT_PATH . '/controller/topPlayasController.php' ;
require_once ROOT_PATH . '/controller/modPlayaController.php' ;



if(isset($_GET['url'])){
    $pagina = $_GET['url'];
    if($pagina == "playas"){
        $votaplayaController = new votaplayaController();
        echo $votaplayaController->getIndex();        
    }else if($pagina=="cerrar"){
        $cerrarController = new cerrarController();
        echo $cerrarController -> getCerrar();
    }else if($pagina=="comentarios"){
        $comenController = new comenController();
        echo $comenController ->getComen();
    }else if($pagina == 'contactanos'){ 
        $contactanosController = new contactanosController();
        echo $contactanosController -> getcontacto();
    }else if($pagina=="convocatorias"){
        $convoController = new convoController();
        echo $convoController -> getConvo();
    }else if($pagina=="ingresar"){
        $ingresarController = new ingresarController();
        echo $ingresarController ->getIngresa();
    }else if($pagina == 'vota'){ 
        $insertaplayaController = new insertaplayaController();
        echo $insertaplayaController ->getVoto();
    }else if($pagina=="nosotros"){
        $nosotrosController = new nosotrosController();
        echo $nosotrosController -> getNosotros();
    }else if($pagina=="perfil"){
        $perfilController = new perfilController();
        echo $perfilController ->getPerfil();
    }else if($pagina=="insertaplaya"){
        $insertaVotController = new insertaVotController();
        echo $insertaVotController ->getinsertaVot();
    }else if($pagina == 'registro'){ 
        $registroController = new registroController();
        echo $registroController -> getRegistro();
    }else if($pagina=="modUsuAdmin"){
        $usuariomodController = new usuariomodController();
        echo $usuariomodController -> getUsuMod();
    }else if($pagina=="usuarios"){
        $usuariosController = new usuariosController();
        echo $usuariosController ->getUsuarios();
    }else if($pagina=="registroAdmin"){
        $registroAdminController = new registroAdminController();
        echo $registroAdminController ->getRegistroAdmin();
    }else if($pagina=="playaRegistro"){
        $pRegistroController = new pRegistroController();
        echo $pRegistroController ->getpRegistro();
    }else if($pagina=="limpias"){
        $topPlayasController = new topPlayasController();
        echo $topPlayasController ->getTopPlayas();
    }else if($pagina == "biotec.php"){
        header('Location: biotec.php');
    }else if($pagina == "modificarplaya"){
        $modPlayaController = new modPlayaController();
        echo $modPlayaController -> getModPlaya();
    }
    else{
        $votaplayaController = new votaplayaController();
        echo $votaplayaController->getIndex();
    }
}else{
    $votaplayaController = new votaplayaController();
    echo $votaplayaController->getIndex();
}

?>