<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2)){
    $usuarios=$this->usuarios;
    // define variables and set to empty values

?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/comenrec.css">
      <link rel="stylesheet" href="css/footer.css">
      <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Usuarios - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="calif">
      <h1>Usuarios</h1>
    </div>
    <table class="table table-striped table-success">
      <thead>
        <tr>
          <th>Usuario</th>
          <th class="inv">Nombre</th>
        </tr>
      </thead>
      <tbody>
      <?php 
      foreach($usuarios as $recorrido)
      {   
        if($permiso ==1)
        {
        ?>
          <tr>
            <td><?php 
            //echo $permiso;
            echo $recorrido->usuario;   ?>
            </td>
            <td class="inv"><?php echo $recorrido->nombre;   ?></td>
            <td>
            <form class="form__reg" action="modUsuAdmin" method="POST" enctype="multipart/form-data">
            <div class="estiloboton">
              <input class="btn__submit" type="submit" name="modificar" value="modificar">    
              <input class="btn__form" type="hidden" name="usu" value="<?php echo $recorrido->usuario; ?>">
            </div>
            </form>
            <form class="form__reg" action="php/eliminarusuario.php" method="POST" enctype="multipart/form-data">
                  <div class="estiloboton">
                  <input class="btn__form" type="hidden" name="usu" value="<?php echo $recorrido->usuario; ?>">
                  <input class="btn__form" type="hidden" name="per" value="<?php echo $recorrido->permiso; ?>">
                  <input class="btn__submit" type="submit" name="eliminar" value="Eliminar">
              </div>
              </form>
            </td>
          </tr>
          <?php 
        }else if(($recorrido->permiso)!=1 and ($recorrido->permiso)!=2)
        {?>
          <tr>
            <td><?php echo $recorrido->usuario;   ?></td>
            <td class="inv"><?php echo $recorrido->nombre;   ?></td>
            <td>
            <form class="form__reg" action="modUsuAdmin" method="POST" enctype="multipart/form-data">
            <div class="estiloboton">
              <input class="btn__submit" type="submit" name="modificar" value="modificar">    
              <input class="btn__form" type="hidden" name="usu" value="<?php echo $recorrido->usuario; ?>">
            </div>
            </form>
            <form class="form__reg" action="php/eliminarusuario.php" method="POST" enctype="multipart/form-data">
                  <div class="estiloboton">
                  <input class="btn__form" type="hidden" name="usu" value="<?php echo $recorrido->usuario; ?>">
                  <input class="btn__form" type="hidden" name="per" value="<?php echo $recorrido->permiso; ?>">
                  <input class="btn__submit" type="submit" name="eliminar" value="Eliminar">
              </div>
              </form>
            </td>
          </tr>
          <?php 
        }
      }?>
      </tbody>
    </table>
        
      
      </div>
                
  <?php include("php/footer.php");}else {     header('Location: nosotros.php');
} ?>
  </body>
</html>