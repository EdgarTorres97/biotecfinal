<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2)){
    $playa=$this->playa;
    // define variables and set to empty values

?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/comenrec.css">
      <link rel="stylesheet" href="css/footer.css">

        <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Bitacora Playas - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="calif">
      <h1>Playas registradas</h1>
    </div>
    <table class="table table-striped table-success">
      <thead>
        <tr>
          <th>Administrador</th>
          <th>Playa</th>
          <th>fecha</th>

        </tr>
      </thead>
      <tbody>
      <?php 
      foreach($playa as $recorrido){        
      ?>
        <tr>
          <td><?php echo $recorrido->nombreAdmin;   ?></td>
          <td><?php echo $recorrido->nombrePlaya;   ?></td>
          <td><?php echo $recorrido->fecha;   ?></td>
          <td>
            <form class="form__reg" action="modificarplaya" method="POST" enctype="multipart/form-data">
            <div class="estiloboton">
              <input class="btn__form" type="hidden" name="idplaya" value="<?php echo $recorrido->idplaya; ?>">
              <input class="btn__submit" type="submit" name="modificar" value="modificar">    
            </div>
            </form>
            <form class="form__reg" action="php/eliminarusuario.php" method="POST" enctype="multipart/form-data">
                  <div class="estiloboton">
                  <input class="btn__form" type="hidden" name="idplaya" value="<?php echo $recorrido->idplaya; ?>">
                  <input class="btn__submit" type="submit" name="eliminar" value="Eliminar">
              </div>
              </form>
            </td>
        </tr>
        <?php 
        } ?>
      </tbody>
    </table>
  <?php include("php/footer.php");}else {     header('Location: nosotros.php');
} ?>
</body>
</html>