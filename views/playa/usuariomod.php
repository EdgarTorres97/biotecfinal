<?php require('php/permiso.php');
//echo $permiso;
if(($permiso == 1 or $permiso ==2)){
  $usu = $_POST['usu'];
  //echo $usu."<br>";
  //obteniendo datos de la tabla usuariosPrivilegiado del usuario elegido
    $sql1="SELECT * FROM usuario WHERE usuario=:usu";
    $resultado1=$base->prepare($sql1);
    $resultado1->execute(array(":usu"=>$usu));
    $result=$resultado1->fetch();
    $nom = $result['nombre'];
    $apePat = $result['apellidoPaterno'];
    $apeMat = $result['apellidoMaterno'];
    $telefono = $result['telefono'];
    $correo = $result['correo'];
    $per = $result['permiso'];
    /*echo $nom."<br>";
    echo $apePat."<br>";
    echo $apeMat."<br>";
    echo $telefono."<br>";
    echo $correo."<br>";*/
  ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/comenrec.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="icon" href="imagenes/logoBiotec.ico">
        <title>Modificar usuarios - Biotec</title>
    </head>
    <body>
    <?php include("php/header.php"); ?>
        <div class="container">
            <div class="form__top">
                <h2>Administrador<span> Biotec</span></h2>
            </div>
            <form class="form__reg" action="php/modUsuAdmin.php" method="POST" enctype="multipart/form-data">
                <h1>Modificar usuario </h1>
                <input type="hidden" name="usu" value="<?php echo $usu; ?>">
                <td><?php echo $usu; ?></td>
                <input class="input" id="descripcion" name="nombre" type="text" placeholder="<?php echo $nom;   ?>" >
                <input class="input" id="descripcion" name="apeP" type="text" placeholder="<?php echo $apePat; ?>" >
                <input class="input" id="descripcion" name="apeM" type="text" placeholder="<?php echo $apeMat;  ?>" >
                <input class="input" id="descripcion" name="telefono" type="text" placeholder="<?php echo $telefono;  ?>" >
                <input class="input" id="descripcion" name="correo" type="text" placeholder="<?php echo $correo;  ?>" >
                <?php if($permiso == 1)
                {
                echo '<input class="input" id="descripcion" name="permiso" type="text" placeholder=' .$per.' >';
                }
                ?>
                <div class="container">
                    <div class="btn__form">
                        <input class="btn__submit" type="submit" value="modificar">
                    </div>
                </div>
            </form>
        </div>
        <?php include("php/footer.php");
        }
        else 
        {     
            header('Location: nosotros.php');
        } ?>
  </body>
  </html> 

