<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2)){
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilo6.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="icon" href="imagenes/logoBiotec.ico">
    <title>Registro Playas - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="container">
      <div class="form__top">
        <h2>Administrador<span> Biotec</span></h2>
      </div>
      <form class="form__reg" action="php/insplaya.php" method="POST" enctype="multipart/form-data">
        <h1>Registro playas </h1>
        <input class="input" id="nombrePlaya" name="titulo" type="text" placeholder="Nombre de la Playa " required autofocus>
        <input class="input" id="descripcion" name="descripcion" type="text" placeholder="Descripcion" required>
        <input class="input" id="imagen" name="laimagen" type="file" placeholder="Imagen " required autofocus>
        <input class="input" id="ubicacion" name="ubicacion" type="text" placeholder="Ubicación" required>
        <div class="container">
            <div class="btn__form">
                <input class="btn__submit" type="submit" value="Registrar">
            </div>
        </div>
      </form>
    </div>
    <?php include("php/footer.php"); }else {     header('Location: nosotros.php');}?>

  </body>
</html>