<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2)){
    $comen=$this->comen;
    // define variables and set to empty values

?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/footer.css">
      <link rel="stylesheet" href="css/comenrec.css">
      <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Comentarios - Biotec</title>
  </head>
  <body>
  <?php include("php/header.php"); ?>
    <div class="calif">
      <h1>Comentarios</h1>
    </div>
    <table class="table table-striped table-success">
      <thead>
        <tr>
          <th>Nombre</th>
          <th class="inv">Correo</th>
          <th class="inv">Telefono</th>
          <th>Comentario</th>
          <th class="inv">Fecha</th>
          </tr>
      </thead>
      <tbody>
      <?php 
          foreach($comen as $recorrido){        
          ?>
      
      <tr>
          <td><?php echo $recorrido->nombre;   ?></td>
          <td class="inv"><?php echo $recorrido->correo; ?></td>
          <td class="inv"><?php echo $recorrido->telefono;  ?></td>
          <td><?php echo $recorrido->comentario;  ?></td>
          <td class="inv"><?php echo $recorrido->fecha;  ?></td>
          </tr>
        <?php 
        } ?>
      </tbody>
    </table>
    <?php include("php/footer.php");}else {     header('Location: nosotros.php');
    } ?>
  </body>
</html>