<?php require('php/permiso.php');
//echo $idusuario;
if(!isset($_SESSION['usuario'])){
    header('Location: ingresar');
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/estilo6.css">
        <link rel="icon" href="imagenes/logoBiotec.ico">
        <title>Cerrar sesión - Biotec</title>
    </head>
    <body>
        <?php include("php/header.php"); ?>
        <form class="form__reg" action="php/cerrar.php" method="POST">
        <div class="form__top">
            <h2>Cerrar sesión de <?php echo $_SESSION['usuario']; ?> <span> Biotec</span></h2>
        </div>
        <div class="container">
            <div class="btn__form">
                <input class="btn__submit" type="submit" value="CERRAR">	                        
            </div>
        </div>
        </form>
        <?php include("php/footer.php"); ?>
    </body>
</html>