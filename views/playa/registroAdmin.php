<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2)){
    // define variables and set to empty values
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/estilo6.css">
        <link rel="icon" href="imagenes/logoBiotec.ico">
        <title>Registro - Biotec</title>
    </head>
    <body>
        <?php include("php/header.php"); ?>
        <div class="container">
            <div class="form__top">
                <h2>Forma parte<span> de Biotec</span></h2>
            </div>		
            <form class="form__reg" action="php/insertarAdmin.php" method="POST">
                <input class="input" id="nombre" name="nombre" type="text" placeholder="Nombre " required autofocus>
                <input class="input" id="apeP" name="apeP" type="text" placeholder="Apellido Paterno" required autofocus>
                <input class="input" id="apeM" name="apeM" type="text" placeholder="Apellido Materno" required autofocus>
                <input class="input" id="correo" name="correo" type="email" placeholder="Email" required>
                <input class="input" id="telefono" name="telefono" type="text" placeholder="Telefono" required>
                <input class="input" id="usuario" name="usuario" type="text" placeholder="Usuario " required autofocus>
                <input class="input" id="contra" name="contra" type="password" placeholder="Contraseña" required>
                <input class="input" id="contra" name="verif" type="password" placeholder="Verificar contraseña" required>
                <div class="btn__form">
                    <input class="btn__submit" type="submit" value="REGISTRAR">
                    <input class="btn__reset" type="reset" value="LIMPIAR">	
                </div>
            </form>
        </div>
        <?php include("php/footer.php"); 
        }else 
        {     
        header('Location: nosotros.php');
        }
        ?>
    </body>
</html>