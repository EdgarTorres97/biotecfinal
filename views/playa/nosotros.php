<?php 
require('php/permiso.php');
//echo $permiso;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/estiloinicio.css">
      <link rel="stylesheet" href="css/footer.css">
      <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Nosotros - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>

    <div class="container">
      <div class="row">
        <div class="col-md-10 col-lg-10">
          <article id="objetivo" >
            <h2 class="parfo">Problemática</h2>
              <hr class="hr">
              <p class="parafo">
              Actualmente el gobierno del estado de 
              Quintana Roo enfrenta la problemática de la contaminación 
              de sus playas producida por el sargazo            
              </p>
          </article>
        </div>
        <div class="col-md-2">
          <p class="frases">
              <b>"La naturaleza siempre lleva los colores del espíritu".</b>
              <br>Autor:Ralph Waldo Emerson 
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 col-lg-10">
          <article id="objetivo" >
            <h2 class="parfo">Promover</h2>
              <hr class="hr">
              <p class="parafo">
              Como amantes de la naturaleza  Buscamos  concientizar a la ciudadania y turismo, sobre el impacto que lleva acabo el sargazo  para esto 
                demostraremos un importante compromiso ante los problemas ambientales y de sostenibilidad. 
              </p>
          </article>
        </div>
        <div class="col-md-2">
          <p class="frases">
              <b>"Mira profundamente en la naturaleza y entonces comprenderás todo mejor".</b>
              <br>Autor:Albert Einstein.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <article id="objetivo" >
            <h2 class="parfo">Información</h2>
            <hr class="hr">
            <p class="parafo">
              Informar sobre el problema que Actualmente enfrentas las playas de cancun y con esto 
              crear conciencia hacia las personas sobre el cuidado de las palyas de cancun llevando 
              acabo Convocatorias para la limpieza de las playas.
              </p>
            </article>
        </div>
        <div class="col-md-2">
          <b>"La naturaleza es el arte de Dios".</b>
          <br>Autor:Dante Alghieri.
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <article id="objetivo" >
            <h2 class="parfo">Seguridad</h2>
            <hr class="hr">
            <p class="parafo">
                  Se seguiran las normas de seguridad en todo momento para garantizar que el personal, el público y 
                  el medio ambiente permanezcan seguros.
            </p>
          </article>
        </div>
        <div class="col-md-2">
            <p>
              <b>" En la naturaleza esta la preservación del mundo".</b>
                <br>Aoutor:Henry David Thoreau.
            </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10">
          <article id="objetivo" >
            <h2 class="parfo">Educación</h2>
              <hr class="hr">
              <p class="parafo">
              Formar actitudes conscientes que permiten el desarrollo 
              personal en armonía con el medio ambiente.
              </p>
            </article>
        </div>
        <div class="col-md-2">
          <p>
            <b>"La naturaleza siempre lleva los colores del espíritu".</b>
          <br>Autor:Ralph Waldo Emerson.
          </p>
        </div>
      </div>
    </div>
    <?php include("php/footer.php"); ?>
</body>
</html>