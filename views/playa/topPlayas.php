<?php require('php/permiso.php');
  $topPlayas=$this->topPlayas;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/estiloplayas.css">
      <link rel="stylesheet" href="css/footer.css">

      <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Playas - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="calif">
      <a href="vota" class="btn"><h1 class="invitamos">Playas con mejor calificación</h1></a>
    </div>
    <?php foreach($topPlayas as $recorrido){ ?>
    <div id="carta" class="card mb-3">
    <?php echo '<img class="card-img-top" src="php/'.$recorrido->imagen.'" alt=""><br>'; ?>
      <div class="card-body">
        <h5 class="card-title"><?php echo $recorrido->nombrePlaya; ?></h5>
        <p class="card-text"><?php echo $recorrido->descripcion;  ?></p>
        <a href="href="<?php echo $recorrido->ubicacion; ?>" class="btn btn-primary">Ubicación</a>
      </div>
    </div>
        <?php } include("php/footer.php"); ?>
  </body>
</html>