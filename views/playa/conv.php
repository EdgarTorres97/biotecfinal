<?php require('php/permiso.php');

    $convoc=$this->convoc;
    // define variables and set to empty values

?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/estiloplayas.css">
      <link rel="stylesheet" href="css/footer.css">
      <link rel="icon" href="imagenes/logoBiotec.ico">
      <title>Convocatorias - Biotec</title>
  </head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="calif">
      <h1>Convocatorias</h1>
    </div>
    <?php foreach($convoc as $recorrido){ ?>
    <div id="carta" class="card mb-3">
      <?php echo '<img class="card-img-top" src="php/'.$recorrido->imagen.'" alt=""><br>'; ?>
      <div class="card-body">
        <h5 class="card-title"><?php echo $recorrido->nombrePlaya; ?></h5>
        <p class="card-text"><?php echo $recorrido->descripcion;  ?></p>
        <a href="<?php echo $recorrido->ubicacion; ?>" class="btn btn-primary">Ubicación</a>
        <h2>Fecha asignada para la convocatoria</h2>
      <div class="calif2">
        <?php for ($i = 0; $i <= 10; $i++) {
              echo $recorrido->fechaConvocatoria[$i];
          }
        ?>
      </div>
    </div>
    </div><?php 
          } ?>

<?php include("php/footer.php"); ?>

</body>
</html>