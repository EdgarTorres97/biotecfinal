<?php require('php/permiso.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/contactanos.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="icon" href="imagenes/logoBiotec.ico">
        <title>Contactanos - Biotec</title>
    </head>
    <body>
    <?php include("php/header.php"); ?>
        <div class="container">
            <div class="form__top">
                <h2>Envianos un comentario<span> de Biotec</span></h2>
            </div>		
            <form class="form__reg" action="php/cont.php" method="POST">
                <input class="input" id="nombre" name="nombre" type="text" placeholder="Nombre " required autofocus>
                <input class="input" id="correo" name="correo" type="email" placeholder="Email" required>
                <input class="input" id="telefono" name="telefono" type="text" placeholder="Telefono" required>
                <input class="input" id="nombre" name="comentario" type="text" placeholder="Comentario" required autofocus>
                <div class="btn__form">
                    <input class="btn__submit" type="submit" value="ENVIAR">
                    <input class="btn__reset" type="reset" value="LIMPIAR">	
                </div>
            </form>
        </div>  
        <?php include("php/footer.php"); ?>
    </body>
</html>