<?php require('php/permiso.php');
if(($permiso == 1 or $permiso ==2 or $permiso == 3)){
    $inserta=$this->inserta;
    // define variables and set to empty values
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/estilovotacion.css">
    <link rel="icon" href="imagenes/logoBiotec.ico">
    <link rel="stylesheet" href="css/footer.css">
    <title>Votaciones - Biotec</title>
</head>
  <body>
    <?php include("php/header.php"); ?>
    <div class="calif">
      <h1>Califique el nivel de contaminación generada por el sargazo en las playas</h1>
    </div>
    <?php 
    foreach($inserta as $recorrido){
    ?>
    <form action="php/votacion.php" method="post">
      <div id="row" class="row row-no-gutters">
        <div id="<?php echo $recorrido->idplaya; ?>" class="col-xs-6 col-md-4" id="">
          <h1>
          <?php 
          echo $recorrido->nombrePlaya;
          ?>
          </h1>
          <?php 
          echo '<div class="col-xs-6 col-md-4">
          <img src="php/'.$recorrido->imagen.'" width="100" height="100"><br>
          </div>
          ';
          ?>
          <div class="radio">
            <label><input type="hidden" name="var" value="<?php echo $recorrido->nombrePlaya;?>"></label>
          </div>
        </div>
        <div class="col-xs-6 col-md-4">
          <div class="radio">
            <label><input type="radio" name="radio" value="limpio">Bajo</label>
          </div>
          <div class="radio">
            <label><input type="radio" name="radio" value="medio">Medio</label>
          </div>
          <div class="radio">
            <label><input type="radio" name="radio" value="sucio">Alto</label>
          </div>
        </div>
        <div class="col-xs-6 col-md-4">   
          <button type="submit" name="submit" class="btn btn-primary">Enviar calificación</button>             
        </div>
      </div>  
    </form>
      <?php 
    }   
   include("php/footer.php"); 
    }else 
    {     
      header('Location: ingresar');
    }
    ?>
  </body>
</html>