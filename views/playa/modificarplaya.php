<?php require('php/permiso.php');
//echo $permiso;
if(($permiso == 1 or $permiso ==2)){
  echo $idPlaya = $_POST['idplaya'];
  //echo $usu."<br>";
  //obteniendo datos de la tabla usuariosPrivilegiado del usuario elegido
    $sql1="SELECT * FROM playas2 WHERE idplaya=:idplaya";
    $resultado1=$base->prepare($sql1);
    $resultado1->execute(array(":idplaya"=>$idPlaya));
    $result=$resultado1->fetch();
    $nom = $result['nombrePlaya'];
    $desc = $result['descripcion'];
    $imagen = $result['imagen'];
    $ubicacion = $result['ubicacion'];

  ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/comenrec.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="icon" href="imagenes/logoBiotec.ico">
        <title>Modificar Playa - Biotec</title>
    </head>
    <body>
    <?php include("php/header.php"); ?>
        
        <div class="container">
      <div class="form__top">
        <h2>Administrador<span> Biotec</span></h2>
      </div>
      <form class="form__reg" action="#" method="POST" enctype="multipart/form-data">
        <h1>Modificar Información de la playa </h1>
        <input class="input" id="nombrePlaya" name="titulo" type="text" placeholder="<?php echo $nom;?> ">
        <input class="input" id="descripcion" name="descripcion" type="text" placeholder="<?php echo $desc;?>">
        <?php echo '<div class="col-xs-6 col-md-4">
          <img src="php/'.$imagen.'" width="100" height="100"><br>
          </div>
          ';?>
        <input class="input" id="imagen" name="laimagen" type="file" placeholder="Imagen ">
        <input class="input" id="ubicacion" name="ubicacion" type="text" placeholder="<?php echo $ubicacion;?>">
        <div class="container">
            <div class="btn__form">
                <input class="btn__submit" type="submit" value="Registrar">
            </div>
        </div>
      </form>
    </div>
        <?php include("php/footer.php");
        }
        else 
        {     
            header('Location: nosotros.php');
        } ?>
  </body>
  </html> 

